import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { createProficienciesList } from './createProficienciesList';
import { createLanguagesList } from './createLanguagesList';

const basicInfoSchema = Yup.object().shape({
    chosenName: Yup.string().notRequired(),
    chosenClass: Yup.string().required('Class is required'),
    chosenRace: Yup.string().required('Race is required'),
  });

const BasicInfo = ()  => {
    const [getName, setName] = useState("");
    const [classOptions, setClassOptions] = useState([]);
    const [raceOptions, setRaceOptions] = useState([]);
    const [getClass, setClass] = useState("");
    const [getRace, setRace] = useState("");
    const [getProficiencies, setProficiencies] = useState([]);
    const [getLanguages, setLanguages] = useState([]);

    useEffect( () => {
        axios.get(`http://www.dnd5eapi.co/api/classes/`)
        .then(res => {
          const classOptions = res.data.results;
          setClassOptions(res.data.results)
        });

        axios.get(`http://www.dnd5eapi.co/api/races/`)
        .then(res => {
          const raceOptions = res.data.results;
          setRaceOptions(res.data.results)
        });

        axios.get(`http://www.dnd5eapi.co/api/proficiencies/`)
        .then(res => {
          const getProficiencies = res.data.results;
          setProficiencies(res.data.results) 
        });

        axios.get(`http://www.dnd5eapi.co/api/languages/`)
        .then(res => {
          const getLanguages = res.data.results;
          setLanguages(res.data.results) 
        });
    }, []);

    const handleSubmit = (e) => {

        console.log(e);

        const chosenClass = e.chosenClass.index;
        const chosenRace = e.chosenRace.index;

        setClass(chosenClass);
        setRace(chosenRace);

        getRaceLanguages(chosenRace);
        getClassProficiencies(chosenClass);
    }

    function getRaceLanguages(raceIndex) {
        axios.get(`http://www.dnd5eapi.co/api/races/` + raceIndex)
        .then(res => {
          const getLanguages = res.data.languages;
          setLanguages(res.data.languages) 
        });
    };

    function getClassProficiencies(classIndex) {
        axios.get(`http://www.dnd5eapi.co/api/classes/` + classIndex)
        .then(res => {
          const getProficiencies = res.data.proficiencies;
          setProficiencies(res.data.proficiencies) 
        });
    };


    const setCharachterName = e => {
        setName(e);
    };



    return(
        <>
            <h2>Choose name, race & class</h2>
            <Formik>
                <Field name="chosenName" render={({field}) =>
                    <span className='p-float-label' style={{ marginTop: '1rem' }}>
                        <InputText id='chosenName' {...field}  value={getName} onChange={e => {
                            setCharachterName(e.target.value)}} style={{ width: '100%' }}/>
                        <label htmlFor='chosenName'>Enter Name</label>
                    </span>
                }/>
            </Formik>
            <Formik
                initialValues={{
                  chosenClass: '',
                  chosenRace: ''
                }}
                validationSchema={basicInfoSchema}
                onSubmit={e => handleSubmit(e)}
                render={() => (
                    <Form>
                        <Field name="chosenRace" render={({field}) =>
                        <Dropdown
                            {...field}
                            style={{ marginTop: '1rem' }}
                            optionLabel="name"
                            options={raceOptions}
                            placeholder="Select D&D Race" style={{ width: '100%', marginTop: '1rem' }}/>} />
                        <ErrorMessage name='chosenRace'/>
                        <br />

                        <Field name="chosenClass" render={({field}) =>
                        <Dropdown
                            {...field}
                            style={{ marginTop: '1rem' }}
                            optionLabel="name"
                            options={classOptions}
                            placeholder="Select D&D Class" style={{ width: '100%', marginTop: '1rem' }}/>}/>
                        <ErrorMessage name='chosenClass'/>
                        <br />
                        <Button label="Select" className="p-button-raised" style={{ marginTop: '1rem' }}/>
                    </Form>
                )}
            />
            <div>
                <h3 className="NameRaceClass">{getName} {(getName && getRace && getClass) ? "the" : null} {getRace} {getClass}</h3>
            </div>
            <div>
                {createProficienciesList(getProficiencies)}
            </div>
            <div>
                {createLanguagesList(getLanguages)}
            </div>
        </>
    );
};

export default BasicInfo;
